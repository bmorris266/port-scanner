# README #

##############################################################
####     Command line Python port scanner                 ####
##############################################################
####               USAGE                                  ####
############################################################## 
#### **For TCP:** portscanner -t <port> -s www.google.com ####
#### **For UDP:** portscanner -u <port> -s www.google.com ####
##############################################################
####   Both protocols support scanning a range of ports   ####
####   If the range flag [-r] is used,                    ####
####   it has to be the first argument on the command     ####
####   line!                                              ####
##############################################################
####   **Sample usage with a range of ports:**            ####
####   portscanner -r -t <port-port> -s www.google.com    ####
####   portscanner -r -u <port-port> -s www.google.com    ####
##############################################################