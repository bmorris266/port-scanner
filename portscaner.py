import socket
import argparse


def port_scan(sock_type, port, host):
	if sock_type == 'SOCK_STREAM':
		s = socket.SOCK_STREAM
	elif sock_type == 'SOCK_DGRAM':
		s = socket.SOCK_DGRAM
	sock = socket.socket(socket.AF_INET, s)
	result = sock.connect_ex((host, port))

	if result == 0:
	   print str(port) + ' is open.'
	else:
	   print str(port) + ' is closed.'


	
comargs = argparse.ArgumentParser(formatter_class=argparse.RawDescriptionHelpFormatter,
description='''If the range flag [-r] is used, 
it has to be the first argument on the command 
line!

Sample usage:
port.py -r -t <port-port> -s www.google.com
-----------------------------------------------''')

comargs.add_argument('-t', '--tcp', metavar='', help='TCP Port')
comargs.add_argument('-u', '--udp', metavar='', help='UDP Port')
comargs.add_argument('-r', '--range', help='Range of ports i.e. 5900-5950', action='store_true')
comargs.add_argument('-s', '--source', metavar='', help='Target location', required=True)
args = comargs.parse_args()

if (args.range):
	if (args.tcp):
		port_list = args.tcp.split('-')
		for port in port_list:
			port_scan('SOCK_STREAM', int(port), args.source)
	elif (args.udp):
		port_list = args.udp.split('-')
		for port in port_list:
			port_scan('SOCK_DGRAM', int(port), args.source)
else:
	if (args.tcp):
		port_scan('SOCK_STREAM', int(args.tcp), args.source)
	elif (args.udp):
		port_scan('SOCK_DGRAM', int(args.udp), args.source)

